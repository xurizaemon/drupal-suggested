# xurizaemon/drupal-suggested

A metapackage to install some fine tools to your Drupal project.

## What is it?

Requiring this metapackage will ensure that you have:

* Build tools
  * [cweagans/composer-patches](https://github.com/cweagans/composer-patches/)
* Coding standards & checks:
  * [drupal/coder](https://www.drupal.org/project/coder)
  * [dealerdirect/phpcodesniffer-composer-installer](https://github.com/Dealerdirect/phpcodesniffer-composer-installer)
  * [phpro/grumphp-shim](https://github.com/phpro/grumphp)
  * [squizlabs/php_codesniffer](https://github.com/squizlabs/PHP_CodeSniffer)
* Behat essentials:
  * [behat/behat](https://github.com/Behat/Behat)
  * [behat/mink](https://github.com/Behat/Mink)
  * [behat/mink-goutte-driver](https://github.com/Behat/mink-goutte-driver)
* Behat + Drupal:
  * [drupal/drupal-driver](https://github.com/jhedstrom/DrupalDriver)
  * [drupal/drupal-extension](https://github.com/jhedstrom/drupalextension)
  * [integratedexperts/behat-steps](https://github.com/integratedexperts/behat-steps)
* Behat + Chromedriver:
  * [dmore/behat-chrome-extension](https://gitlab.com/DMore/behat-chrome-extension/)
* Behat + Screenshots:
  * [integratedexperts/behat-screenshot](https://github.com/integratedexperts/behat-screenshot)
  * [rpkamp/mailhog-behat-extension](https://github.com/rpkamp/mailhog-behat-extension)

## Prep for install

Since you're in the "private beta" program, you'll need to add this "private repo" VCS to your project's composer.json first:

`composer config repositories.drupal-suggested vcs https://gitlab.com/xurizaemon/drupal-suggested.git`

If this gets blessed, we can release it on Packagist and that won't be required.

## Installation

`composer require xurizaemon/drupal-suggested`

## Compatibility

This package is tested with:

- Composer v1 and v2 (official docker images)
- Drupal 8.8, 8.9, 9.0
